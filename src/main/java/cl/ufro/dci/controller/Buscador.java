package cl.ufro.dci.controller;

import cl.ufro.dci.model.ValorBolsa;

import java.util.ArrayList;

public class Buscador {

    public ValorBolsa busqueda(ArrayList<ValorBolsa> valores, int dia, int mes){
        ValorBolsa valorBolsa=new ValorBolsa("",0,0);
        for (ValorBolsa valor:valores) {
            if (valor.getDia()==dia && valor.getMes()==mes){
                valorBolsa=valor;
            }
        }
        return valorBolsa;
    }
}
