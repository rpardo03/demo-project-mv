package cl.ufro.dci;
import cl.ufro.dci.controller.Buscador;
import cl.ufro.dci.helper.Pagina;
import cl.ufro.dci.model.ValorBolsa;

import java.util.ArrayList;

public class App 
{
    public static void main( String[] args ){
        Pagina pag=new Pagina();
        pag.loadContentPage();
        ArrayList<ValorBolsa> valores=pag.getContent();
        Buscador buscador=new Buscador();
        System.out.println(buscador.busqueda(valores,12,5).getValor());
        System.out.println(buscador.busqueda(valores,12,5));
    }
}
