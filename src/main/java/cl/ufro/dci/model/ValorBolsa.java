package cl.ufro.dci.model;

public class ValorBolsa {
    private String valor;
    private int mes;
    private int dia;

    public ValorBolsa(String valor, int mes, int dia) {
        this.valor = valor;
        this.mes = mes;
        this.dia = dia;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    @Override
    public String toString() {
        return "ValorBolsa{" +
                "valor='" + valor + '\'' +
                ", mes=" + mes +
                ", dia=" + dia +
                '}';
    }
}
