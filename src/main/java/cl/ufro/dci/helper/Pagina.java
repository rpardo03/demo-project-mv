package cl.ufro.dci.helper;

import cl.ufro.dci.model.ValorBolsa;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class Pagina {
    private String url;
    private Document doc;

    public Pagina() {
        this.url = "https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=";
    }

    public void loadContentPage() {
        try {
            // Aquí creamos un objeto Document y usamos JSoup para obtener el sitio web
            this.doc = Jsoup.connect(this.url).get();

            //En caso de errores IO, queremos que los mensajes se escriban en la consola.
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que selecciona la tabla y extrae sus datos para luego guardarlos en una lista
     * @return ArrayList<ValorBolsa>
     */
    public ArrayList<ValorBolsa> getContent(){
        ArrayList<ValorBolsa> valores = new ArrayList<>();
        try{
            Element table = this.doc.getElementById("gr");
            Elements rows = table.select("tr");
            for (int i = 0; i < rows.size(); i++) {
                Elements td = rows.get(i).select("td");
                Elements span = td.select("span");
                for (int j=0;j<span.size(); j++){
                    String valor = span.get(j).text();
                    ValorBolsa vb = new ValorBolsa(valor,(j+1),i);
                    valores.add(vb);
                }
            }
            return valores;
        }catch(Exception e){
            return new ArrayList<ValorBolsa>();
        }
    }

}